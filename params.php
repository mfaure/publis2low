<?php


	/* Paramétres de connexion à la base de données Mysql */
	$host='localhost';
	$mysql_ident='';
	$mysql_mdp='';
	$base='';


	// Change le dossier courant utile lors du lancement des tâches cron
	chdir(dirname(__FILE__));


	// Préfixe des tables mysql
	$pref_tab='rs_';

	
	// Préfixe du certificat Exemple : $cert="test_", les fichiers seront test_client.pem, test_key.pem et test_ca.pem
	$cert="test_";
	// Mot de passe donné lors de la création des fichiers pem
	$pass="Mot de passe";
	

	// Url vers la plateforme s2low que vous voulez atteindre
	define('URL', 'https://www.s2low.org/');
	
	// Insee de la commune ou EPCI dans le cas où l'insee n'est pas spécifiée
	$insee_par_defaut='57999';
	
	// Clé d'accès à l'essemble des Actes
	$cle_ctrl='ma clé pour accéder à tous actes';
		
?>